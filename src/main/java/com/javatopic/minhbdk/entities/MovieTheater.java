package com.javatopic.minhbdk.entities;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "MovieTheaters")
public class MovieTheater {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int rowNo;
    private int colNo;

    @OneToMany(mappedBy = "movieTheater")
    private Set<Seat> seatSet;

    @OneToMany(mappedBy = "movieTheater")
    private Set<ShowTime> showTimeSet;

    public Long getId() {
        return id;
    }

    public int getRowNo() {
        return rowNo;
    }

    public void setRowNo(int rowNo) {
        this.rowNo = rowNo;
    }

    public int getColNo() {
        return colNo;
    }

    public void setColNo(int colNo) {
        this.colNo = colNo;
    }

    public Set<Seat> getSeatSet() {
        return seatSet;
    }

    public void setSeatSet(Set<Seat> seatSet) {
        this.seatSet = seatSet;
    }

    public Set<ShowTime> getShowTimeSet() {
        return showTimeSet;
    }

    public void setShowTimeSet(Set<ShowTime> showTimeSet) {
        this.showTimeSet = showTimeSet;
    }
}
