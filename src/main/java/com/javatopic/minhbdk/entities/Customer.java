package com.javatopic.minhbdk.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity(name="Customers")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String fullName;

    @Column(nullable = false)
    private String password;

    private String identityCard;

    @Column(nullable = false)
    private String email;

    private String address;

    @Column(columnDefinition = "DATE", nullable = false)
    private Date birthday;

    private int gender;

    @OneToMany(mappedBy = "customer")
    private Set<TicketBooked> ticketBookedSet;

    public Customer() {
    }

    public Long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public Set<TicketBooked> getTicketBookedSet() {
        return ticketBookedSet;
    }

    public void setTicketBookedSet(Set<TicketBooked> ticketBookedSet) {
        this.ticketBookedSet = ticketBookedSet;
    }
}
